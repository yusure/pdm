<?php
/**
 * [pdm] (C)2014 markdream Inc.
 *
 * $Id: IndexAction.class.php  2014-10-24 下午12:29:09 pony_chiang $
 */
defined ( 'PDM_URL' ) or exit ( 'Access Denied' );

class IndexAction extends PDMAction {

	function __construct() {
		parent::__construct ();
	}

	function index() {
		$this->display ();
	}

// 	function updatepwd() {
// 		$old_key = PDM_MAIN_CODE;
// 		$new_key = PDM_NEW_CODE;
		
// 		// update user Code
// 		$mode_user = M ( 'users' );
// 		$mapUser ['email_auth'] = 1;
// 		$dataUserList = $mode_user->field ( 'id,password,auth_code' )->where ( $mapUser )->select ();
// 		foreach ( $dataUserList as $k => $v ) {
// 			$old_pwd = pdm_decode ( $v ['password'] );
// 			$old_auth = pdm_decode ( $v ['auth_code'] );
// 			$data ['password'] = pdm_encode ( $old_pwd, PDM_NEW_CODE );
// 			$data ['auth_code'] = pdm_encode ( $old_auth, PDM_NEW_CODE );
// 			$map ['id'] = $v ['id'];
// 			$mode_user->where ( $map )->save ( $data );
// // 			$tmpAuthCode=pdm_decode($v['auth_code'],PDM_NEW_CODE);
// 			// 更新密码
// 			// 读取用户所有密码
// 			$mode_password=M('password');
// 			$mapPwd['userid']=$v['id'];
// 			$dataPwdList=$mode_password->where($mapPwd)->field('id,uname,pwd')->select();
// 			foreach ($dataPwdList as $p=>$q){
// 				$unameOld=pdm_decode($q['uname'],$v ['auth_code']);
// 				$pwdOld=pdm_decode($q['pwd'],$v ['auth_code']);
// 				$dataPwd['pwd']=pdm_encode ( $pwdOld, $old_auth  );
// 				$dataPwd['uname']=pdm_encode ( $unameOld, $old_auth  );
// 				$mapNewPwd['id']=$q['id'];
// 				$mode_password->where($mapNewPwd)->save($dataPwd);
// 				echo $mode_password->getLastSql();
// 			}
			
// 		}
		
// 	}
	
// 	function red(){
// 		$mode_password=M('password');
// 		$mapPwd['userid']=1;
// 		$dataPwdList=$mode_password->where($mapPwd)->field('id,uname,pwd')->select();
// 		foreach ($dataPwdList as $p=>$q){
// 			$unameOld=pdm_decode($q['pwd'],'1a356fdf640a82e80eb897842a19450e');
// 			dump($unameOld.time().' --- ');
// 		}
// 	}
	
// 	function blue(){
// 		$mode_password=M('users');
// 		$mapUser ['email_auth'] = 1;
// 		$dataUserList = $mode_password->field ( 'id,password,auth_code' )->where ( $mapUser )->select ();
// 		foreach ($dataUserList as $p=>$q){
// 			$unameOld=pdm_decode($q['auth_code'],PDM_NEW_CODE);
// 			dump($unameOld.time().' --- ');
// 		}
// 	}
}